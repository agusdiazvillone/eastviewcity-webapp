VERSION=1.0.0
IMAGE_NAME=eastviewcity-webapp

json:
	json -I -f package.json -e 'this.version="$(VERSION)"' 

docker: json	
	sudo docker build -t $(IMAGE_NAME) . --no-cache
	sudo docker tag $(IMAGE_NAME):latest $(IMAGE_NAME):$(VERSION)

exec_docker:
	sudo docker run -p 3000:3000 \
	$(IMAGE_NAME)
