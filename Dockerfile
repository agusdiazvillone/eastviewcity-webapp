FROM node:12.2.0-alpine


# make the 'app' folder the current working directory
WORKDIR /app

# copy 'package-lock.json' (if available)
COPY package-lock.json ./

# copy both 'package.json'(if available)
COPY package.json ./

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

# install project dependencies
RUN npm install 

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .

# build app for production with minification
RUN npm run build

EXPOSE 3000
CMD ["npm", "start"]