import { Router } from 'express'
import request from 'request-promise'
import { parseToQuery } from '../lib/utils'
import { server } from '../server.config'

const router = Router()

router.get('/', (req, res, next) => {
	let params = req.query ? `/?${parseToQuery(req.query)}` : ''
	let token = req.session.user.token
	let options = {
		method: 'GET',
		url: `${server.urlApi}/api/Sale${params}`,
		headers: {
			'Authorization': `Bearer ${token}`
		},
		json: true
	}

	request(options)
		.then((data) => {
			try {
				res.status(200).json(JSON.parse(data))
			} catch (error) {
				res.status(200).json(data)
			}
		})
		.catch((err) => {
			next(err)
		})
})

router.get('/:id', (req, res, next) => {
	let token = req.session.user.token
	let options = {
		method: 'GET',
		url: `${server.urlApi}/api/SaleDetail/?SaleId=${req.params.id}`,
		headers: {
			'Authorization': `Bearer ${token}`
		},
		json: true
	}

	request(options)
		.then((data) => {
			try {
				res.status(200).json(JSON.parse(data))
			} catch (error) {
				res.status(200).json(data)
			}
		})
		.catch((err) => {
			next(err)
		})
})
export default router
