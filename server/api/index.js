import { Router } from 'express'
import session from '../lib/session'

import users from './users'
import auth from './auth'
import dashboard from './dashboard'
import dashboardV2 from './v2/dashboardV2'
import sales from './sales'
import salesV2 from './v2/salesV2'
import ordersV2 from './v2/ordersV2'
import consolidatedV2 from './v2/consolidatedV2'
import orders from './orders'
import customers from './customers'
import events from './events'
import boxV2 from './v2/boxV2'
import box from './box'
import providers from './providers'
import products from './products'
import productsV2 from './v2/productsV2'
import clientsV2 from './v2/clientsV2'
import report from './report'
import categories from './categories'
import info from './info'
import apps from './apps'

const router = Router()

router.use('/users', session, users)
router.use('/auth', auth)
router.use('/dashboard', session, dashboard)
router.use('/dashboardV2', session, dashboardV2)
router.use('/sales', session, sales)
router.use('/salesV2', session, salesV2)
router.use('/ordersV2', session, ordersV2)
router.use('/consolidatedV2', session, consolidatedV2)
router.use('/orders', session, orders)
router.use('/customers', session, customers)
router.use('/events', session, events)
router.use('/box', session, box)
router.use('/boxV2', session, boxV2)
router.use('/providers', session, providers)
router.use('/products', session, products)
router.use('/productsV2', session, productsV2)
router.use('/clientsV2', session, clientsV2)
router.use('/report', session, report)
router.use('/categories', session, categories)
router.use('/apps', session, apps)
router.use('/locations'/*, session */, info)

export default router
