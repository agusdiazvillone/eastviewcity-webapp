require('dotenv').config()
const env = process.env.NODE_ENV || 'development'

const config = {
	env: {
		current: env,
		is: {
			development: env === 'development',
			testing: env === 'testing',
			production: env === 'production'
		}
	},
	server: {
		host: process.env.HOST || '0.0.0.0',
		port: process.env.NODE_PORT || process.env.PORT || 3000,
		urlApi: process.env.URL_API || 'http://lbl-bistro-netcore-webapi-prod-ecbb75bded592eb9.elb.us-east-1.amazonaws.com:5000'
		// newUrlApi: process.env.NEW_URL_API || 'http://lbl-bistro-netcore-webapi-prod-ecbb75bded592eb9.elb.us-east-1.amazonaws.com:5000'
	},
	cookie: {
		session: {
			keys: [process.env.COOKIE_SESSION_KEY || '-8jd*#9o&iQAoCxd52hhvxCfuNMcQBXP']
		}
	}
}

module.exports = Object.freeze(config)
